"""

"""
from org.xmcda import XMCDA
import org.xmcda

XMCDA_v2_TAG_FOR_FILENAME = {
    # output name -> XMCDA v2 tag
    'alternativesValues': 'alternativesValues',
    'messages': 'methodMessages',
}

XMCDA_v3_TAG_FOR_FILENAME = {
    # output name -> XMCDA v3 tag
    'alternativesValues': 'alternativesValues',
    'messages': 'programExecutionResult',
}


def xmcda_v3_tag(outputFilename):
    return XMCDA_v3_TAG_FOR_FILENAME[outputFilename]


def xmcda_v2_tag(outputFilename):
    return XMCDA_v2_TAG_FOR_FILENAME[outputFilename]


def convert(results, xmcda_execution_results): # TODO
    """
    Converts the outputs of the computation to XMCDA objects

    :param results:
    :param ...:
    :param xmcda_execution_results:
    :return: a map with keys being the names of the outputs, and their corresponding XMCDA v3 values (NOT including 'messages.xml')
    """
    # In our case, results is simply the result of simpleWeightedSum,
    # that is to say, alternatives'ids mapped to their values.
    alternatives_values = results

    # translate the results into XMCDA v3
    xmcda_alternatives_values = XMCDA() # for XMCDA tag: alternativesValues

    x_alternatives_values = org.xmcda.AlternativesValues()

    for alternative_id in alternatives_values.keys():
        x_alternatives_values.put(org.xmcda.Alternative(alternative_id),
                                  alternatives_values[alternative_id])

    # insert it into the XMCDA object
    xmcda_alternatives_values.alternativesValuesList.add(x_alternatives_values)

    return {
        'alternativesValues': xmcda_alternatives_values,
        }
