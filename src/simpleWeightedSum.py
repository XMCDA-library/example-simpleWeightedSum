
def calculate_simple_weighted_sum(inputs):
    """
    Calculates the weighted sum.

    :param inputs: contains the performance table and the weights
    :param weights: a dictionary { criterion id -> weight }
    :return: a dictionary { alternative id -> double }
    """
    # here the performance table is a dictionary
    # mapping (alternative id,criterion id) to their values
    performance_table = inputs.performance_table

    # and the weights are stored in a dictionary as well,
    # mapping criteria' ids to their respective weights
    weights = inputs.weights

    weighted_sum = {} # the weighted sum: { alternative's id -> double }
    for (alternative_id, criterion_id), value in performance_table.items():
        if not weighted_sum.has_key(alternative_id):
            weighted_sum[alternative_id] = 0.0

        weighted_sum[alternative_id] += value * weights[criterion_id]

    return weighted_sum
