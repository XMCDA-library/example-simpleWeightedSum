from java.lang import Throwable

import os
import utils
import org.xmcda.XMCDA

WEIGHTED_SUM = "weightedSum"
NORMALIZED_WEIGHTED_SUM = "normalizedWeightedSum"
AGGREGATION_OPERATORS = (WEIGHTED_SUM, NORMALIZED_WEIGHTED_SUM)

class Inputs:
    """
    Class Inputs gathers all the information retrieved from the XMCDA inputs,
    for use by the weighted sum.
    """
    def __init__(self):
        self.performance_table={} # { (alternative id, criterion id) -> value }
        self.weights={} # { criterion -> weights }
        self.operator=None


def check_and_extract_inputs(xmcda, xmcda_exec_results):
    """

    :param xmcda: the XMCDA object containing all the program's data
    :param xmcda_exec_results: the execution results for this program
    :return: the input object filled with data extracted from XMCDA
    """
    inputs = Inputs()
    check_inputs(inputs, xmcda, xmcda_exec_results)

    if not (xmcda_exec_results.isOk() or xmcda_exec_results.isWarning()):
        return None

    return extract_inputs(inputs, xmcda, xmcda_exec_results)


def check_inputs(inputs, xmcda, xmcda_exec_results):

    # Check the performance table object
    if len(xmcda.performanceTablesList) == 0:
        xmcda_exec_results.addError("No performance table has been supplied")
    elif len(xmcda.performanceTablesList) > 1:
        xmcda_exec_results.addError("More than one performance table has been supplied")
    else:
        p = xmcda.performanceTablesList.get(0)

        if p.isEmpty():
            xmcda_exec_results.addError("The performance table is empty")
        elif p.hasMissingValues():
            xmcda_exec_results.addError("The performance table has missing values")
        # Anyhow, each of its values must be numeric
        if not p.isNumeric():
            xmcda_exec_results.addError("The performance table must contain numeric values only")

    # Check weights
    if xmcda.criteriaValuesList.size() == 0:
        xmcda_exec_results.addError("Weights have not been supplied")
    elif xmcda.criteriaValuesList.size() > 1:
        xmcda_exec_results.addError("More than one single criteriaWeights has been supplied")
    elif not xmcda.criteriaValuesList.get(0).isNumeric():
        xmcda_exec_results.addError('The weights must be numeric values only')

    # Check parameters
    while True: # avoid nested if/then/else by using 'break' instead
        if len(xmcda.programParametersList) > 1:
            xmcda_exec_results.addError("Only one programParameter is expected")
            break

        if len(xmcda.programParametersList) == 0:
            xmcda_exec_results.addError("No programParameter found")
            break

        prg_param = xmcda.programParametersList.get(0).get(0)
        if not "operator" == prg_param.id():
            xmcda_exec_results.addError("Invalid parameter w/ id '%s'" % prg_param.id())
            break

        if prg_param.getValues() is None or (prg_param.getValues() is not None and len(prg_param.getValues()) != 1):
            xmcda_exec_results.addError("Parameter operator must have a single (label) value only")
            break

        operator = prg_param.getValues().get(0).getValue()
        if operator not in AGGREGATION_OPERATORS:
            err='Invalid value for parameter operator, it must be a label, '+\
                'possible values are: ' + ','.join(AGGREGATION_OPERATORS)
            xmcda_exec_results.addError(err)
            operator = None

        inputs.operator = operator
        break # end check parameters

    # check that perf. table and weights have the same set of criteria' ids
    # first, get the alternatives & criteria from the performance table
    alternatives_ids = []
    criteria_ids = []
    xmcda_perf_table = xmcda.performanceTablesList.get(0)

    for x_alternative in xmcda_perf_table.getAlternatives():
        alternatives_ids.append(x_alternative.id())
    for x_criterion in xmcda_perf_table.getCriteria():
        criteria_ids.append(x_criterion.id())

    # NB: we already checked the performance table for emptiness:
    #     both lists cannot be empty

    # Check weights' criteria ids against performance table's ones
    weights_criteria_ids = []
    for x_criterion in xmcda.criteriaValuesList.get(0).getCriteria():
        weights_criteria_ids.append(x_criterion.id())
    # get the intersection
    intersection = [ criterion for criterion in weights_criteria_ids
                                if criterion in criteria_ids ]
    if len(intersection) != len(criteria_ids):
        err="The list of criteria ids in performanceTable and criteriaWeights do not match"
        xmcda_exec_results.addError(err)

    if not (xmcda_exec_results.isOk() or xmcda_exec_results.isWarning()):
        return None

    return inputs

def extract_inputs(inputs, xmcda, xmcda_execution_results):
    "transform XMCDA inputs into what we need for the algorithm"

    xmcda_perf_table = xmcda.performanceTablesList.get(0)

    # Build input.performance_table
    x_perf_table = xmcda.performanceTablesList.get(0)
    x_perf_table = x_perf_table.asDouble() # convert numeric values to floats

    performance_table = inputs.performance_table
    for x_alternative in x_perf_table.getAlternatives():
        for x_criterion in x_perf_table.getCriteria():
            # Get the value attached to the alternative, create it if necessary
            value = x_perf_table.getValue(x_alternative, x_criterion)
            performance_table[(x_alternative.id(), x_criterion.id())] = value

    # Build inputs.weights
    weights = inputs.weights
    for criterion in xmcda_perf_table.getCriteria():
        x_value=xmcda.criteriaValuesList.get(0).get(criterion).get(0).getValue()
        weights[criterion.id()] = float(x_value)

    # Last, normalize the weights if we were told to.
    # Remember that the simpleWeightedSum() just takes a performance table
    # and criteria' weights as input!
    if inputs.operator == NORMALIZED_WEIGHTED_SUM:
        weights_sum = 0.0
        for v in weights.values():
            weights_sum += v
        for criterion_id in weights.keys():
            weights[criterion_id] = weights[criterion_id] / weights_sum

    return inputs
